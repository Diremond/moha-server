package fr.iutinfo.skeleton.common.dto;

import java.util.List;

public class EventDto {
    private int id;
    private String name;
    private String sport;

    private String beginningDate;
    private String endDate;

    private int eventOwner;
    private List<Integer> invited;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(String beginningDate) {
        this.beginningDate = beginningDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getEventOwner() {
        return eventOwner;
    }

    public void setEventOwner(int eventOwner) {
        this.eventOwner = eventOwner;
    }

    public List<Integer> getInvited() {
        return invited;
    }

    public void setInvited(List<Integer> invited) {
        this.invited = invited;
    }
}
