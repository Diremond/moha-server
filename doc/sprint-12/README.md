# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Formulaire, ajouter un créneau, afficher un calendrier, connection entre appli et BDD

## Ce que nous allons faire durant le prochain sprint
Créer un calendrier, afficher événements par utilisateur, Authentification mobile

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Le bon fonctionnement des formulaires, l'affichage d'un calendrier simple, création d'un événement
### Qu'avons nous observé ?
Il y a des choses faites mais beaucoup de choses prévue pas terminé
### Quelle décision prenons nous suite à cette expérience ?
Avancer sur le mobile et le calendrier
### Qu'allons nous tester durant les 2 prochaines heures ?
Créer un calendrier, afficher événements par utilisateur, Authentification mobile
### À quoi verra-t-on que celà à fonctionné ?
Seconnecter à l'appli ? Voir le calendrier ? Ajouter un événements
