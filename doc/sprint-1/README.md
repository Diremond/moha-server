# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Mise en place du GIT et Initialisation du serveur REST

## Ce que nous allons faire durant le prochain sprint
Mise en place de la BDD, Page d'accueil, Page d'inscription

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
X
### Qu'avons nous observé ? 
Pair Programming fonctionne bien. Tâches trop importantes, Mauvaise estimation de la difficulté des tâches
### Quelle décision prenons nous suite à cette expérience ? 
Mieux organiser les tâches, mieux estimer la difficulté
### Qu'allons nous tester durant les 2 prochaines heures ? 
Mise en place de la BDD, Page d'accueil, Page d'inscription
### À quoi verra-t-on que celà à fonctionné ?
Peut-on créer un utilisateur ? Peut-on se rendre sur le site ?
