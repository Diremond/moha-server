# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Page vitrine, Page login, Page profil

## Ce que nous allons faire durant le prochain sprint
Installation API REST, Page accueil V2, Structure SPA, Mise en page accueil, Différents layout mobile

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
On peut accéder aux différentes pages
### Qu'avons nous observé ? 
Pair Programming fonctionne bien.Beaucoup de choses lancées sans être finie
### Quelle décision prenons nous suite à cette expérience ? 
Découper plus les différentes tâches
### Qu'allons nous tester durant les 2 prochaines heures ? 
Installation API REST, Page accueil V2, Structure SPA, Mise en page accueil, Différents layout mobile
### À quoi verra-t-on que celà à fonctionné ?
L'accueil à-t-il changé (graphique, fonctionnalité) ? Peut-on accéder à l'appli et aux différentes pages ?
