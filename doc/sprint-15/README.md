# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Affichage calendrier, RESR API Participants, Connecter profile appli à BDD

## Ce que nous allons faire durant le prochain sprint
Intégration de toutes les fonctionnalités dans la SPA, ajouter un event sur le calendrier

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
L'affichage du calendrier, voir son profile sur mobile
### Qu'avons nous observé ?
On a réussit à finir ce qu'on avait prévu
### Quelle décision prenons nous suite à cette expérience ?
On change rien
### Qu'allons nous tester durant les 2 prochaines heures ?
L'intégration des différentes fonctionnalités dans la SPA, ajouter un event sur le calendrier
### À quoi verra-t-on que celà à fonctionné ?
A-t-on accès à toutes les fonctionnalités depuis la SPA ?
