# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Afficher le calendrier, layout profile mobile

## Ce que nous allons faire durant le prochain sprint
Affichage calendrier, RESR API Participants, Connecter profile appli à BDD

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
On a un calendrier dynamique
### Qu'avons nous observé ?
Avancement correct
### Quelle décision prenons nous suite à cette expérience ?
Permettre l'insertion d'événements dans le calendrier
### Qu'allons nous tester durant les 2 prochaines heures ?
Affichage calendrier, RESR API Participants, Connecter profile appli à BDD
### À quoi verra-t-on que celà à fonctionné ?
Voir un profile sur mobile ? modifier le calendrier
