# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Installation API REST, Page accueil V2, Layout authentification et login

## Ce que nous allons faire durant le prochain sprint
Layout mobile inscription, accueil et mobile, Structure SPA, Mise en page accueil, lien page d'inscription à la BDD

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Nouvelle page et Appli mobile
### Qu'avons nous observé ? 
Beaucoup de choses lancées sans être finie
### Quelle décision prenons nous suite à cette expérience ? 
Découper plus les différentes tâches
### Qu'allons nous tester durant les 2 prochaines heures ? 
Layout mobile inscription, accueil et mobile, Structure SPA, Mise en page accueil, lien page d'inscription à la BDD
### À quoi verra-t-on que celà à fonctionné ?
Est ce qu'on a une SPA ? Peut-on inscrire un nouvel utilisateur ? 
