# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Mise en place de la BDD, Page d'accueil, Page d'inscription

## Ce que nous allons faire durant le prochain sprint
Page vitrine, Page login, Page profil, Page accueil V2, Installation API REST

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
On peut accéder au site web et on a les différents onglets (Home, Entreprises, Particuliers, ...), on peut remplir le formulaire d'inscription mais il n'est pas connecté à la base.
	La BDD est mise en place, on peut ajouter un nouvel utilisateur 
### Qu'avons nous observé ? 
Pair Programming fonctionne bien. Meilleur partage des tâches, bonne estimation de la difficulté des tâches
### Quelle décision prenons nous suite à cette expérience ? 
On continue en essayant d'améliorer la quantité des rendus
### Qu'allons nous tester durant les 2 prochaines heures ? 
Page vitrine, Page login, Page profil, Page accueil V2, Installation API REST
### À quoi verra-t-on que celà à fonctionné ?
Peut-on se rendre sur le site ? Se connecter ? Ajouter un utilisateur via le site ? Accéder à son profil ?
