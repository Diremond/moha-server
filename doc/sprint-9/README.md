# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Lien login à la BDD, lien entre inscription appli et BDD, Intégration pages dans la SPA

## Ce que nous allons faire durant le prochain sprint
Lien inscription appli, api evenements, front-end/SPA

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
La connection (login) au site, les nouvelles fonctionnalités
### Qu'avons nous observé ?
On peut se connecter mais l'appli prend plus de temps
### Quelle décision prenons nous suite à cette expérience ?
Finir la connection entre al BDD et l'appli, commencer le travail sur le calendrier
### Qu'allons nous tester durant les 2 prochaines heures ?
Lien inscription appli, api evenements, front-end/SPA
### À quoi verra-t-on que celà à fonctionné ?
S'inscrire avec l'appli ? Nouvelles fonctionnalité/affichage fonctionnel ?
