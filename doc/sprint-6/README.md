# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Layout mobile inscription, Mise en page accueil, lien page d'inscription à la BDD

## Ce que nous allons faire durant le prochain sprint
Layout mobile inscription, Mise en page accueil, lien page d'inscription à la BDD

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Les nouvelles fonctionnalités du site et l'appli mobile
### Qu'avons nous observé ? 
Tâches plus importante que prévu
### Quelle décision prenons nous suite à cette expérience ? 
Finir les productions en cours
### Qu'allons nous tester durant les 2 prochaines heures ? 
Layout mobile inscription, Mise en page accueil, lien page d'inscription à la BDD
### À quoi verra-t-on que celà à fonctionné ?
Les nouvelles fonctionnalité sont-elles dispo sur le site ? Le forumulaire d'inscription est-il dispo sur le site ? L'appli est-elle accessible ?
