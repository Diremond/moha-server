# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Layout mobile inscription, Mise en page accueil, lien page d'inscription à la BDD

## Ce que nous allons faire durant le prochain sprint
Lien entre profil & login à la BDD, lien entre appli et BDD

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Le lien entre la BDD et le site, l'appli
### Qu'avons nous observé ? 
Connection réussit à la base, l'appli tourne mais n'est connecttée à la base
### Quelle décision prenons nous suite à cette expérience ? 
Avancer sur la connection entre les différentes pages et la base de donnée
### Qu'allons nous tester durant les 2 prochaines heures ? 
Lien entre profil & login à la BDD, lien entre appli et BDD
### À quoi verra-t-on que celà à fonctionné ?
Peut-on se connecter ? voir son profil ? Voir ces infos sur l'appli ?
