# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Lien inscription appli, api evenements, lien infos perso

## Ce que nous allons faire durant le prochain sprint
Lien entre login appli et BDD, Créer un calendrier, ajouter un créneau, afficher événements par utilisateur

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
CSS page login, Authentification
### Qu'avons nous observé ?
On a pas réussit à faire ce qu'on avait prévu mais on a fait d'autres choses qui n'était pas prévues
### Quelle décision prenons nous suite à cette expérience ?
Faire des recherches sur android et sur le calendrier
### Qu'allons nous tester durant les 2 prochaines heures ?
Lien entre login appli et BDD, Créer un calendrier, ajouter un créneau, afficher événements par utilisateur
### À quoi verra-t-on que celà à fonctionné ?
Seconnecter à l'appli ? Voir le calendrier ? Créer un événements ? Ajouter un événements
