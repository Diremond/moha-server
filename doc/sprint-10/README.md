# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
API evenements, front-end/SPA, lien connection et insccription sur le site

## Ce que nous allons faire durant le prochain sprint
Lien inscription appli, api evenements, lien infos perso

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
On peut se connecter et s'inscrire sur le site avec els données de la BDD
### Qu'avons nous observé ?
Toujours des difficultés avec la connexion entre l'appli et la BDD, le site commence à bien avancer
### Quelle décision prenons nous suite à cette expérience ?
Demander de l'aide pour android, continuer le site web
### Qu'allons nous tester durant les 2 prochaines heures ?
Lien inscription appli, api evenements, lien infos perso
### À quoi verra-t-on que celà à fonctionné ?
Voir ses infos perso ? Créer un événements ?
