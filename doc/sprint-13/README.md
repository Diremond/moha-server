# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Créer un calendrier, Authentification mobile

## Ce que nous allons faire durant le prochain sprint
Afficher event par utilisateurs, ajouter participants aux event, layout profile mobile

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
On peut créer et afficher un calendrier basique, on peut se connecter sur l'appli
### Qu'avons nous observé ?
Bon avancement
### Quelle décision prenons nous suite à cette expérience ?
Accentuer la charge de travail pour le calendrier
### Qu'allons nous tester durant les 2 prochaines heures ?
Afficher event par utilisateurs, ajouter participants aux event, layout profile mobile
### À quoi verra-t-on que celà à fonctionné ?
Voir un profile sur mobile ? Afficher un event pour un utilisateur, ajouter un event à un utilisateur
