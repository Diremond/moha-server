# Rétrospective de sprint

Nom du scrum master du sprint : Antoine Moreau

## Ce que nous avons fait durant ce sprint
Lien entre profil à la BDD

## Ce que nous allons faire durant le prochain sprint
Lien login à la BDD, lien entre inscription appli et BDD, Intégration pages dans la SPA

## PDCA 
### Qu'avons nous testé durant ce sprint ? 
Connection à son profile
### Qu'avons nous observé ?
On peut accéder à son profile
### Quelle décision prenons nous suite à cette expérience ?
Connecter le login à la BDD, finir la connection entre l'inscription et l'appli
### Qu'allons nous tester durant les 2 prochaines heures ?
Lien login à la BDD, lien entre inscription appli et BDD, Intégration pages dans la SPA
### À quoi verra-t-on que celà à fonctionné ?
Peut-on se connecter ? S'inscrire avec l'appli ?
