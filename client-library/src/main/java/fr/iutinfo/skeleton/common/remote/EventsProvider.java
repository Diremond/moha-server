package fr.iutinfo.skeleton.common.remote;

import fr.iutinfo.skeleton.common.dto.EventDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class EventsProvider {
    final static Logger logger = LoggerFactory.getLogger(EventsProvider.class);
    private String baseURL;

    public EventsProvider(String baseURL) {
        this.baseURL = baseURL;
    }

    public List<EventDto> readAllEvents() {
        try {
            return ClientBuilder.newClient()//
                    .target(baseURL + "event/")
                    .request()
                    .get(new GenericType<List<EventDto>>() {
                    });
        } catch (Exception e) {
            String message = ClientBuilder.newClient()
                    .target(baseURL + "event/")
                    .request()
                    .get(String.class);

            logger.error(e.getMessage());
            throw new RuntimeException(message);
        }
    }

    public EventDto addEvent(EventDto eventDto) {
        logger.debug("Create user : " + eventDto.getName());
        Entity<EventDto> userEntity = Entity.entity(eventDto, MediaType.APPLICATION_JSON);

        return ClientBuilder.newClient()
                .target(baseURL + "user/")
                .request()
                .post(userEntity)
                .readEntity(EventDto.class);
    }

    public EventDto readEvent(String name) {
        String url = baseURL + "event/" + name;
        logger.debug("Reade url : " + url);

        return ClientBuilder.newClient()//
                .target(url)
                .request()
                .get(EventDto.class);
    }
}
