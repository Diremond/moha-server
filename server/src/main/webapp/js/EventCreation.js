$(document).ready(function() {
    $("#createEvent").click(function() {
        postEvent(
            $("#name").val(),
            $("#sport").val(),
            $("#beginningDate").val(),
            $("#endDate").val(),
            $("#eventOwner").val()
        )
    })
});

function postEvent(eventName, sport, beginningDate, endDate, eventOwner) {
    console.log(eventName);
    postEventGeneric(eventName, sport, beginningDate, endDate, eventOwner, "/api/v1/event")
}

function postEventGeneric(name, sport, beginningDate, endDate, eventOwner, url) {
    console.log(beginningDate);
    console.log("postEventGeneric " + url)
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: url,
        dataType: 'json',
        data : JSON.stringify({
            "name" : name,
            "sport" : sport,
            "beginningDate" : beginningDate,
            "endDate" : endDate,
            "eventOwner" : eventOwner
        })
    })
}