$(document).ready(function() {
	$("#connectButton").click(function () {
		login();

	    $.ajax({
            type: 'GET',
            url: '/api/v1/user/' + $('#login').val(),
            success: function(data) {
                showProfile(data);
            }
        });
	});





});

function participationClick() {
    postParticipation(
        $('#participantId').val(),
        $('#eventId').val()
    )
}

function postParticipation(user, event) {
    $.ajax({
        type : 'POST',
        contentType : 'application/json',
        dataType : 'json',
        url : '/api/v1/participation',

        data : JSON.stringify({
            "user" : user,
            "event" : event
        }),
        success: console.log("bravo post participation"),
   });
}

function login() {
	getWithAuthorizationHeader("/api/v1/login/", function(data){
	    $("#login_form").hide();
	});
}

function showProfile(data) {
    //$("#profile").load('participation.html')
    $("#profile").html("<p>" +
                        "Login: " + data.name +
                        "<br>Email: " + data.email +
                        "<br>Prénom et Nom: " + data.name + " " + data.lastname +
                        "</p>");

    $.ajax({
        type: 'GET',
        url: '/api/v1/participation/user/' + data.id,
        success: function(data) {
            showUserEvents(data);
        }
    })

}

function showUserEvents(data) {
    $("#profile").append("<p>My Events</p>")
    for (var i = 0; i < data.length; i++) {
        $('#profile').append('Event ID: ' + data[i].event)
    }
    $("#profile").append('ID Participant: <input type="text" id="participantId" placeholder="ID Participant" />'
                            + 'ID Evenement: <input type="text" id="eventId" placeholder="ID Evenement" />'
                            + '<button type="text" id="participationButton" onclick="participationClick()" >Créer la participation</button>')
}
