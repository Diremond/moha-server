package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.EventDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Event {
    final static Logger logger = LoggerFactory.getLogger(Event.class);

    private int id;
    private String name;
    private String sport;

    private String beginningDate;
    private String endDate;

    private int eventOwner;
    private List<Integer> invited;

    public Event() {}

    public Event(int id, String eventName, String sport) {
        this.id = id;
        this.name = eventName;
        this.sport = sport;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String eventName) {
        this.name = eventName;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(String beginningDate) {
        this.beginningDate = beginningDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getEventOwner() {
        return eventOwner;
    }

    public void setEventOwner(int eventOwner) {
        this.eventOwner = eventOwner;
    }

    public List<Integer> getInvited() {
        return invited;
    }

    public void setInvited(List<Integer> invited) {
        this.invited = invited;
    }

    public EventDto convertToDto() {
        EventDto dto = new EventDto();
        dto.setId(this.getId());
        dto.setName(this.getName());
        dto.setSport(this.getSport());
        dto.setBeginningDate(this.getBeginningDate());
        dto.setEndDate(this.getEndDate());
        dto.setEventOwner(this.getEventOwner());
        dto.setInvited(this.getInvited());
        return dto;
    }

    public void initFromDto(EventDto eventDto) {
        setId(eventDto.getId());
        setName(eventDto.getName());
        setSport(eventDto.getSport());
        setBeginningDate(eventDto.getBeginningDate());
        setEndDate(eventDto.getEndDate());
        setEventOwner(eventDto.getEventOwner());
        setInvited(eventDto.getInvited());
    }
}
