package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.ParticipationDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.*;

@Path("/participation")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ParticipationResource {
    private static ParticipationDao dao = getDbi().open(ParticipationDao.class);

    public ParticipationResource() throws SQLException {
        if (!tableExist("participations")) {
            logger.debug("Create 'participations' table");
            dao.createParticipantTable();
            dao.insert(new Participation(1, 1, 7));
        }
    }

    @GET
    public List<ParticipationDto> getParticipations() {
        List<Participation> participations = dao.all();
        return participations.stream().map(Participation::convertToDto).collect(Collectors.toList());
    }

    @POST
    public ParticipationDto createParticipation(ParticipationDto dto) {
        Participation participation = new Participation();
        participation.initFromDto(dto);
        int id = dao.insert(participation);
        dto.setId(id);
        return dto;
    }

    @GET
    @Path("/user/{id}")
    public List<ParticipationDto> getParticipationsByUser(@PathParam("id") int id) {
        return dao.findByUser(id).stream().map(Participation::convertToDto).collect(Collectors.toList());
    }
}
