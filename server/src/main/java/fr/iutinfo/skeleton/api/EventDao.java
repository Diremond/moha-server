package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface EventDao {
    @SqlUpdate("CREATE TABLE events (ID INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255), sport VARCHAR(255), " +
            "beginningDate DATE, endDate DATE, eventOwner INTEGER)")
    void createEventTable();

    @SqlUpdate("INSERT INTO events (name, sport, beginningDate, endDate, eventOwner) VALUES (:name, :sport, :beginningDate, :endDate, :eventOwner)")
    @GetGeneratedKeys
    int insert(@BindBean Event event);

    @SqlQuery("SELECT * FROM events ORDER BY ID")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Event> all();

    @SqlQuery("SELECT * FROM events WHERE id = :id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    Event findById(@Bind("id") int id);

    @SqlQuery("SELECT * FROM events WHERE eventOwner = :eventOwner")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Event> getEventByOrganisation(@Bind("eventOwner") int eventOwner);

    @SqlUpdate("drop table if exists events")
    void dropEvent();
}
