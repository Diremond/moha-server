package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.ParticipationDto;

public class Participation {
    private int id;
    private int user;
    private int event;

    public Participation() {}

    public Participation(int id, int user, int event) {
        this.id = id;
        this.user = user;
        this.event = event;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    public ParticipationDto convertToDto() {
        ParticipationDto dto = new ParticipationDto();
        dto.setId(this.getId());
        dto.setUser(this.getUser());
        dto.setEvent(this.getEvent());
        return dto;
    }

    public void initFromDto(ParticipationDto dto) {
        setId(dto.getId());
        setUser(dto.getUser());
        setEvent(dto.getEvent());
    }
}
