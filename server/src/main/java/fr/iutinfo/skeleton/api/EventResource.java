package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.EventDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static fr.iutinfo.skeleton.api.BDDFactory.getDbi;
import static fr.iutinfo.skeleton.api.BDDFactory.tableExist;

@Path("/event")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EventResource {
    final static Logger logger = LoggerFactory.getLogger(EventResource.class);
    private static EventDao dao = getDbi().open(EventDao.class);

    public EventResource() throws SQLException {
        if (!tableExist("events")) {
            logger.debug("Create 'events' table");
            dao.createEventTable();
            dao.insert(new Event(1, "Sport 1", "Golf"));
        }
    }

    @POST
    public EventDto createEvent(EventDto eventDto) {
        Event event = new Event();
        event.initFromDto(eventDto);
        int id = dao.insert(event);
        eventDto.setId(id);
        return eventDto;
    }

    @GET
    public List<EventDto> getEvents() {
        List<Event> events = dao.all();
        return events.stream().map(Event::convertToDto).collect(Collectors.toList());
    }

    @GET
    @Path("/{id}")
    public EventDto getEvent(@PathParam("id") int id) {
        return dao.findById(id).convertToDto();
    }

    @GET
    @Path("/owner/{id}")
    public List<EventDto> getEventByOrganisation(@PathParam("id") int id) {
        return dao.getEventByOrganisation(id).stream().map(Event::convertToDto).collect(Collectors.toList());
    }

}
