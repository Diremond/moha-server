package fr.iutinfo.skeleton.api;

import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

import java.util.List;

public interface ParticipationDao {
    @SqlUpdate("CREATE TABLE participations (ID INTEGER PRIMARY KEY AUTOINCREMENT, user INTEGER, event INTEGER)")
    void createParticipantTable();

    @SqlUpdate("INSERT INTO participations (user, event) VALUES (:user, :event)")
    @GetGeneratedKeys
    int insert(@BindBean Participation participation);

    @SqlQuery("SELECT * FROM participations ORDER BY id")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Participation> all();

    @SqlQuery("SELECT * FROM participations WHERE user = :user")
    @RegisterMapperFactory(BeanMapperFactory.class)
    List<Participation> findByUser(@Bind("user") int user);

    @SqlUpdate("DROP TABLE IF EXISTS events")
    void dropParticipations();

}
