package fr.iutinfo.skeleton.api;

import fr.iutinfo.skeleton.common.dto.EventDto;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

public class EventResourceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new Api();
    }

    @Test
    public void should_create_event_in_db() {
        EventDao dao = BDDFactory.getDbi().open(EventDao.class);
        Event event = new Event();
        event.setName("Sport 2000");
        dao.dropEvent();
        dao.createEventTable();
        dao.insert(event);
        Event eventLoaded = dao.findById(1);
        Assert.assertEquals("Sport 2000", eventLoaded.getName());
    }

    @Test
    public void should_create_event() {
        EventDto event = new EventDto();
        event.setSport("Sport");
        event.setName("Sport 2000");
        Entity<EventDto> entity = Entity.entity(event, MediaType.APPLICATION_JSON);
        EventDto eventDto = target("/event").request().post(entity).readEntity(EventDto.class);
        Assert.assertEquals("Sport 2000",eventDto.getName());
    }
}
